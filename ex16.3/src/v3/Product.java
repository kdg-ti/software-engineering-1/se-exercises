package v3;

public class Product {
    ProductCategorie categorie;
    Money prijs;
    Percentage suikerGehalte;

    public Money getPrijs() {
        return prijs;
    }

    public Percentage getSuikerGehalte() {
        return suikerGehalte;
    }

    public ProductCategorie getCategorie() {
        return categorie;
    }

    public Money berekenSuikerTax(){
        Percentage schaal = categorie.getSuikerTaxSchaal();
        return new Money(prijs.value * suikerGehalte.value * schaal.value);
    }
}
