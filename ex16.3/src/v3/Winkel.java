package v3;

import java.util.HashMap;
import java.util.Map;
public class Winkel {
    Map<Integer, Product> products = new HashMap<>();
    public Money berekenSuikerTax(int productID) {
        Product p = products.get(productID);
        return p.berekenSuikerTax();
    }
}
