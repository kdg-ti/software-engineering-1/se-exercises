package v2;

import java.util.HashMap;
import java.util.Map;

public class ProductGamma {
    Map<Integer, Product> products = new HashMap<>();

    public void verwijderProduct(int productID){
        products.remove(productID);
    }

    public Money berekenSuikerTax(int productID){
        Product p = products.get(productID);
        Money prijs = p.getPrijs();
        Percentage suikerGehalte = p.getSuikerGehalte();
        ProductCategorie categorie = p.getCategorie();
        Percentage suikerTaxSchaal = categorie.getSuikerTaxSchaal();

        Money tax = new Money(prijs.value * suikerGehalte.value * suikerTaxSchaal.value);
        return tax;
    }
}
