package v2;

public class BelastingController {
    Winkel w = new Winkel();
    public Money berekenSuikerTax(int productId){
        ProductGamma productGamma = w.getProductGamma();
        return productGamma.berekenSuikerTax(productId);
    }
}
