package v1;

import java.util.HashMap;
import java.util.Map;
public class Winkel {
    Map<Integer,Product> products = new HashMap<>();
    public Money berekenSuikerTax(int productID) {
        Product p = products.get(productID);
        Money prijs = p.getPrijs();
        Percentage suikerGehalte = p.getSuikerGehalte();
        ProductCategorie categorie = p.getCategorie();
        Percentage suikerTaxSchaal = categorie.getSuikerTaxSchaal();

        Money tax = new Money(prijs.value * suikerGehalte.value * suikerTaxSchaal.value);
        return tax;
    }

    public Klant getKlant(){
        return null;
    }

    public void verwijderProduct(int productID){
    }
    public Verkoop getVerkoop(int verkoopID){
        return null;
    }
}
