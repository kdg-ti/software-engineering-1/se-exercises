package v1;

public class Product {
    ProductCategorie categorie;
    Money prijs;
    Percentage suikerGehalte;

    public Money getPrijs() {
        return prijs;
    }

    public Percentage getSuikerGehalte() {
        return suikerGehalte;
    }

    public ProductCategorie getCategorie() {
        return categorie;
    }
}
